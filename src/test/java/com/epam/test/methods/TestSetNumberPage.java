package com.epam.test.methods;

import com.epam.parserpage.Core;
import com.epam.parserpage.ioexception.NullUrlIOException;
import com.epam.parserpage.ioexception.NumberLessRangeIOException;
import com.epam.parserpage.ioexception.ValidateStringIOException;
import com.epam.parserpage.page.HTML;
import com.epam.parserpage.parser.ParserHTML;
import com.epam.parserpage.parser.type.Bash;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TestSetNumberPage {


    @Test (expected = NumberLessRangeIOException.class)
    public void checkExceptionSetSiteNameBashSetPageZero() throws ValidateStringIOException, NumberLessRangeIOException, NullUrlIOException {
        Core core = new Core();
        HTML html = HTML.getInstance();
        ParserHTML parserHTML = ParserHTML.getInstance(core);

        parserHTML.setUrl("https://bash.im/quote/");
        html.setUrl("https://bash.im/quote/");
        html.setPage(0);

        assertEquals(new Bash().parsing(html.getTextPage()), null);
    }

    @Test
    public void checkQuoteSetSiteNameBashSetPage1() throws ValidateStringIOException, NumberLessRangeIOException, NullUrlIOException {
        Core core = new Core();
        HTML html = HTML.getInstance();
        ParserHTML parserHTML = ParserHTML.getInstance(core);

        parserHTML.setUrl("https://bash.im/quote/");
        html.setUrl("https://bash.im/quote/");
        html.setPage(1);

        assertEquals(new Bash().parsing(html.getTextPage()), "<Ares> ppdv, все юниксы очень дружелюбны.. они просто очень разборчивы в друзьях ;)");
    }

    @Test
    public void checkQuoteSetSiteNameBashSetPage11() throws ValidateStringIOException, NumberLessRangeIOException, NullUrlIOException {
        Core core = new Core();
        HTML html = HTML.getInstance();
        ParserHTML parserHTML = ParserHTML.getInstance(core);

        parserHTML.setUrl("https://bash.im/quote/");
        html.setUrl("https://bash.im/quote/");
        html.setPage(11);

        assertEquals(new Bash().parsing(html.getTextPage()),
                "<Zoi> она мне - \"ЗОЙ, ЭТО Я!\" \n" +
                        "<Zoi> Надо было ответить \"Нет, Зой -- это я\"... :)");
    }

    @Test
    public void checkQuoteSetSiteNameBashSetPage111() throws ValidateStringIOException, NumberLessRangeIOException, NullUrlIOException {
        Core core = new Core();
        HTML html = HTML.getInstance();
        ParserHTML parserHTML = ParserHTML.getInstance(core);

        parserHTML.setUrl("https://bash.im/quote/");
        html.setUrl("https://bash.im/quote/");
        html.setPage(111);

        assertEquals(new Bash().parsing(html.getTextPage()),
                "<Spacoom[MSK]> юзать общественный компьютер все равно, что юзать общественное нижнее белье");
    }

    @Test
    public void checkQuoteSetSiteNameBashSetPage1111() throws ValidateStringIOException, NumberLessRangeIOException, NullUrlIOException {
        Core core = new Core();
        HTML html = HTML.getInstance();
        ParserHTML parserHTML = ParserHTML.getInstance(core);

        parserHTML.setUrl("https://bash.im/quote/");
        html.setUrl("https://bash.im/quote/");
        html.setPage(1111);

        assertEquals(new Bash().parsing(html.getTextPage()),
                "<hmepas> пока читал Ваш дуратский bash.org.ru забыл зачем сел за комп ;(* А ведь что-то умное-вечное-полезное хотел сделать....");
    }

    @Test (expected = ValidateStringIOException.class)
    public void checkExceptionSetSiteNameBashSetPage11111() throws ValidateStringIOException, NumberLessRangeIOException, NullUrlIOException {
        Core core = new Core();
        HTML html = HTML.getInstance();
        ParserHTML parserHTML = ParserHTML.getInstance(core);

        parserHTML.setUrl("https://bash.im/quote/");
        html.setUrl("https://bash.im/quote/");
        html.setPage(11111);

        assertEquals(new Bash().parsing(html.getTextPage()), null);
    }

    @Test (expected = NumberLessRangeIOException.class)
    public void checkExceptionSetSiteNameBashSetPageMinus1() throws ValidateStringIOException, NumberLessRangeIOException, NullUrlIOException {
        Core core = new Core();
        HTML html = HTML.getInstance();
        ParserHTML parserHTML = ParserHTML.getInstance(core);

        parserHTML.setUrl("https://bash.im/quote/");
        html.setUrl("https://bash.im/quote/");
        html.setPage(-1);

        assertEquals(new Bash().parsing(html.getTextPage()), null);
    }

    @Test (expected = NumberLessRangeIOException.class)
    public void checkExceptionSetSiteNameBashSetPageMinus1111111111() throws ValidateStringIOException, NumberLessRangeIOException, NullUrlIOException {
        Core core = new Core();
        HTML html = HTML.getInstance();
        ParserHTML parserHTML = ParserHTML.getInstance(core);

        parserHTML.setUrl("https://bash.im/quote/");
        html.setUrl("https://bash.im/quote/");
        html.setPage(-1111111111);

        assertEquals(new Bash().parsing(html.getTextPage()), null);
    }
}
