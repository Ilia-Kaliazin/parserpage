package com.epam.test.methods;

import com.epam.parserpage.Core;
import com.epam.parserpage.ioexception.NullUrlIOException;
import com.epam.parserpage.ioexception.NumberLessRangeIOException;
import com.epam.parserpage.ioexception.ValidateStringIOException;
import com.epam.parserpage.page.HTML;
import com.epam.parserpage.parser.ParserHTML;
import com.epam.parserpage.parser.type.Bash;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TestParserHTML {

    @Test
    public void checkExceptionSetSiteNameNullSetStringParsingNull()
    {
        Core core = new Core();
        ParserHTML parserHTML = ParserHTML.getInstance(core);
        parserHTML.setTextPage(null);
        parserHTML.setUrl(null);
        parserHTML.run();
    }

    @Test
    public void checkNoHostSetSiteNameABCDSetStringParsingNull()
    {
        Core core = new Core();
        ParserHTML parserHTML = ParserHTML.getInstance(core);
        parserHTML.setTextPage(null);
        parserHTML.setUrl("ABCD");
        parserHTML.run();
    }

    @Test
    public void checkExceptionSetSiteNameNullSetStringParsingABCD()
    {
        Core core = new Core();
        ParserHTML parserHTML = ParserHTML.getInstance(core);
        parserHTML.setTextPage("ABCD");
        parserHTML.setUrl(null);
        parserHTML.run();
    }

    @Test
    public void checkExceptionParsingSetSiteNameBashSetStringParsingABCD()
    {
        Core core = new Core();
        ParserHTML parserHTML = ParserHTML.getInstance(core);
        parserHTML.setTextPage("ABCD");
        parserHTML.setUrl("Bash.im");
        parserHTML.run();
    }

    @Test
    public void parsingSetSiteNameBashSetStringParsingPage11() throws ValidateStringIOException, NumberLessRangeIOException, NullUrlIOException {
        Core core = new Core();
        HTML html = HTML.getInstance();
        ParserHTML parserHTML = ParserHTML.getInstance(core);

        parserHTML.setUrl("https://bash.im/quote/");
        html.setUrl("https://bash.im/quote/");
        html.setPage(11);

        assertEquals(new Bash().parsing(html.getTextPage()) ,
                "<Zoi> она мне - \"ЗОЙ, ЭТО Я!\" \n" +
                        "<Zoi> Надо было ответить \"Нет, Зой -- это я\"... :)");
    }

    @Test
    public void parsingSetSiteNameBashSetStringParsingPage123() throws ValidateStringIOException, NumberLessRangeIOException, NullUrlIOException {
        Core core = new Core();
        HTML html = HTML.getInstance();
        ParserHTML parserHTML = ParserHTML.getInstance(core);

        parserHTML.setUrl("https://bash.im/quote/");
        html.setUrl("https://bash.im/quote/");
        html.setPage(123);

        assertEquals(new Bash().parsing(html.getTextPage()) ,
                "<}{Lighter> знаете что такое линукс?\n" +
                "<}{Lighter> мне тут юзверь выдал\n" +
                "<}{Lighter> \"ну это же типо виндовз, но программы надо выполнять, набором букв, а не мышкой щёлкать\"");
    }
}
