package com.epam.test.connection;

import com.epam.parserpage.page.HTML;

import org.junit.Test;

import java.io.IOException;
import java.net.UnknownHostException;

import static org.junit.Assert.assertTrue;

public class TestConnectionToSite {

    @Test
    public void checkConnectionToSiteBash() throws IOException {
        HTML html = HTML.getInstance();
        html.setUrl("https://bash.im/quote/");

        assertTrue(html.verifyConnect(html.httpConnection()));
    }

    @Test (expected = UnknownHostException.class)
    public void checkExceptionConnectToSiteNull() throws IOException {
        HTML html = HTML.getInstance();
        html.setUrl("http://null");

        html.verifyConnect(html.httpConnection());
    }
}
