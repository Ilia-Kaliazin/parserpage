package com.epam;

import com.epam.test.connection.TestConnectionToSite;
import com.epam.test.methods.TestParserHTML;
import com.epam.test.methods.TestSetNumberPage;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;


@RunWith(Suite.class)
@Suite.SuiteClasses({
        TestConnectionToSite.class,
        TestSetNumberPage.class,
        TestParserHTML.class
})

public class AppTest 
{
}
