package com.epam.parserpage.page;

import com.epam.parserpage.ioexception.NullUrlIOException;
import com.epam.parserpage.ioexception.NumberLessRangeIOException;

import java.io.IOException;
import java.net.ConnectException;
import java.net.URL;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;

public class HTML {
    private String url;
    private int numberPage;
    private static HTML instance;

    public static HTML getInstance() {
        if (instance != null) return instance;
        return new HTML();
    }

    /**  Get page html */
    private String getPageHTML() {
        try {
            HttpURLConnection httpURLConnection = httpConnection();
            verifyConnect(httpURLConnection);

            StringBuffer response = new StringBuffer();
            BufferedReader in = new BufferedReader(new InputStreamReader(httpURLConnection.getInputStream()));

            String inputLine = in.readLine();

            for (; inputLine != null ; inputLine = in.readLine()) {
                response.append(inputLine);
            }

            in.close();
            httpURLConnection.disconnect();
            return response.toString();
        } catch (Exception e) {
            System.err.println(e.getMessage());
            return null;
        }
    }

    private void checkValidateValue () throws NullUrlIOException, NumberLessRangeIOException {
        if(url == null) throw new NullUrlIOException("Url site null.");
        if(numberPage <= 0) throw new NumberLessRangeIOException("Incorrect number page.");
    }

    public String getTextPage () throws NullUrlIOException, NumberLessRangeIOException {
        checkValidateValue();
        return getPageHTML();
    }

    public HttpURLConnection httpConnection () throws IOException {
        URL obj = new URL(url + numberPage);
        HttpURLConnection connection = (HttpURLConnection) obj.openConnection();

        connection.setRequestMethod("GET");

        return connection;
    }

    public boolean verifyConnect (HttpURLConnection connection) throws IOException {
        if(200 <= connection.getResponseCode() && connection.getResponseCode() <= 399) return true;
        throw new ConnectException("Server http not respond.");
    }

    public void setUrl(String string){
        url = string;
    }

    public void setPage(int i){
        numberPage = i;
    }
}
