package com.epam.parserpage.ioexception;

import java.io.IOException;

public class NullStringParsingIOException extends IOException {
    public NullStringParsingIOException() {
    }

    public NullStringParsingIOException(String message) {
        super(message);
    }

    public NullStringParsingIOException(String message, Throwable cause) {
        super(message, cause);
    }

    public NullStringParsingIOException(Throwable cause) {
        super(cause);
    }
}
