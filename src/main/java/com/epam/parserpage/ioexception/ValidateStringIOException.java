package com.epam.parserpage.ioexception;

import java.io.IOException;

public class ValidateStringIOException extends IOException {
    public ValidateStringIOException() {
    }

    public ValidateStringIOException(String message) {
        super(message);
    }

    public ValidateStringIOException(String message, Throwable cause) {
        super(message, cause);
    }

    public ValidateStringIOException(Throwable cause) {
        super(cause);
    }
}
