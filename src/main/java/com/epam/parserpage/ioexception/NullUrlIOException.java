package com.epam.parserpage.ioexception;

import java.io.IOException;

public class NullUrlIOException extends IOException {
    public NullUrlIOException() {
    }

    public NullUrlIOException(String message) {
        super(message);
    }

    public NullUrlIOException(String message, Throwable cause) {
        super(message, cause);
    }

    public NullUrlIOException(Throwable cause) {
        super(cause);
    }
}
