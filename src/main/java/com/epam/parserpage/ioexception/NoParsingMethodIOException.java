package com.epam.parserpage.ioexception;

import java.io.IOException;

public class NoParsingMethodIOException extends IOException {
    public NoParsingMethodIOException() {
    }

    public NoParsingMethodIOException(String message) {
        super(message);
    }

    public NoParsingMethodIOException(String message, Throwable cause) {
        super(message, cause);
    }

    public NoParsingMethodIOException(Throwable cause) {
        super(cause);
    }
}
