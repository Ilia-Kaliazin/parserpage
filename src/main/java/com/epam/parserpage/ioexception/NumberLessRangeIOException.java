package com.epam.parserpage.ioexception;

import java.io.IOException;

public class NumberLessRangeIOException extends IOException {
    public NumberLessRangeIOException() {
    }

    public NumberLessRangeIOException(String message) {
        super(message);
    }

    public NumberLessRangeIOException(String message, Throwable cause) {
        super(message, cause);
    }

    public NumberLessRangeIOException(Throwable cause) {
        super(cause);
    }
}
