package com.epam.parserpage;

import com.epam.parserpage.ioexception.NullUrlIOException;
import com.epam.parserpage.ioexception.NumberLessRangeIOException;
import com.epam.parserpage.page.HTML;
import com.epam.parserpage.parser.ParserHTML;
import com.epam.parserpage.configuration.Config;
import com.epam.parserpage.console.Communication;

public class Core {
    private final HTML html;
    private final Config config;
    private final ParserHTML parserHTML;
    private final Communication communication;

    public Core () {
        html = HTML.getInstance();
        parserHTML = ParserHTML.getInstance(this);
        communication = Communication.getInstance(this);
        config = Config.getInstance(this);
    }

    public void run (){
        communication.run();
        config.run();
    }

    public void doesNotExistFile() {
        config.createNewFileConfig(communication.getUrlFromUser());
    }

    /** --HTML-- */
    public void setPageHTML(String string) {
        parserHTML.setTextPage(string);
        parserHTML.run();
    }

    /** --ParserOut-- */
    public void setQuote(String string) {
        communication.printToUser(String.format("[Quote]\n|\n|  %s\n|\n[Done]", string.replace("\n", "\n|  ")));
    }

    /** --Console-- */
    public void setNumberPage(int i) {
        html.setPage(i);
    }

    /** --Configuration-- */
    public void setUrl(String string) throws NumberLessRangeIOException, NullUrlIOException {
        parserHTML.setUrl(string);
        html.setUrl(string);

        setPageHTML(html.getTextPage());
    }

    /** --Exception-- */
    public void onIOException(Exception e) {
        communication.printToUser(e.getMessage());
    }

    public void onIOException(String string) {
        communication.printToUser(string);
    }
}
