package com.epam.parserpage.console;

import com.epam.parserpage.Core;
import com.epam.parserpage.ioexception.NumberLessRangeIOException;

import java.io.IOException;
import java.util.Scanner;

public class Communication {
    private int numberPage;
    private final Core core;
    private final Scanner scanner;
    private static Communication instance;

    private Communication (Core core){
        this.core = core;
        this.scanner = new Scanner(System.in);
    }

    public static Communication getInstance(Core core){
        if(instance != null) return instance;
        return new Communication(core);
    }

    private int parseNumberPage (String string) throws NumberLessRangeIOException {
        int parseInt = Integer.parseInt(string);
        if(parseInt <= 0) throw new NumberLessRangeIOException("Incorrect number page.\n");
        return parseInt;
    }

    private boolean requestPageAtUser (){
        printToUser("Give me number: ");
        try {
            numberPage = parseNumberPage(inputValue(null));
            return true;
        } catch (IOException e) {
            core.onIOException(e);
        } catch (NumberFormatException e){
            core.onIOException("Incorrect number.\n");
        }
        return false;
    }

    public String inputValue (String string) {
        return (string == null) ? scanner.next() : string;
    }

    public void run () {
        while (true){
            if(requestPageAtUser()){
                core.setNumberPage(numberPage);
                break;
            }
        }
    }

    public String getUrlFromUser () {
        printToUser("Give me url site (example: https://bash.im/quote/): ");
        return inputValue(null);
    }

    public void printToUser (String string) {
        System.out.print(string);
    }
}
