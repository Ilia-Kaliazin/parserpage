package com.epam.parserpage.parser;

import com.epam.parserpage.Core;
import com.epam.parserpage.ioexception.NoParsingMethodIOException;
import com.epam.parserpage.ioexception.NullStringParsingIOException;
import com.epam.parserpage.ioexception.NullUrlIOException;
import com.epam.parserpage.ioexception.ValidateStringIOException;
import com.epam.parserpage.parser.type.Bash;

import java.io.IOException;

public class ParserHTML {
    private String textPage;
    private String urlName;
    private final Core core;
    private static ParserHTML instance;

    private ParserHTML(Core core) {
        this.core = core;
    }

    public static ParserHTML getInstance(Core core) {
        if (instance != null) return instance;
        return new ParserHTML(core);
    }

    private void verifyValue () throws NullUrlIOException, NullStringParsingIOException {
        if(urlName == null) throw new NullUrlIOException("UrlName null");
        if(textPage == null) throw new NullStringParsingIOException("Text page to parsing null");
    }

    private void initObjectUrl() throws NullUrlIOException, NullStringParsingIOException, NoParsingMethodIOException, ValidateStringIOException {
        verifyValue();

        if(urlName.toLowerCase().contains("bash.im")) {
            core.setQuote(new Bash().parsing(textPage));
            return;
        }

        throw new NoParsingMethodIOException("No host indication.");
    }

    public void run () {
        try {
            initObjectUrl();
        } catch (IOException e){
            core.onIOException(e);
        }
    }

    public void setTextPage(String string){
        this.textPage = string;
    }

    public void setUrl(String string) {
        this.urlName = string;
    }
}
