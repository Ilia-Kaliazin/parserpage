package com.epam.parserpage.parser.type;

import com.epam.parserpage.ioexception.ValidateStringIOException;

public class Bash {
    private boolean validateString (String string) throws ValidateStringIOException {
        if (string == null || !string.contains("quote__body")) throw new ValidateStringIOException("(Bash) validateString : Wrong string.");
        return true;
    }

    private boolean validateQuote (String string) throws ValidateStringIOException {
        if (string.indexOf("quote__body") != string.lastIndexOf("quote__body")) throw new ValidateStringIOException("(Bash) validateQuote : Not found page.");
        return true;
    }

    private int checkImageInStringQuote (String string) {
        return string.contains("quote__strips") ? string.indexOf("quote__strips") - 16 : string.indexOf("<footer class=\"quote__footer\">") - 16;
    }

    private String getSubStringQuote (String string) {   //quote__strips
        return string.substring(string.indexOf("<div class=\"quote__body\">") + 25, checkImageInStringQuote(string));
    }

    private String removeSpace (String string) {
        return string.trim();
    }

    private String replaceHTMLTags (String string) {
        return string.replace("<br>", "\n")
                .replace("<br />", "\n");
    }

    private String replaceAdHocTag (String string) {
        return string.replace("&lt;", "<")
                .replace("&gt;", ">")
                .replace("&quot;", "\"");
    }

    private String replaceLastLineBreak (String string) {
        return (string.lastIndexOf("\n") == string.length() - 1) ? string.substring(0, string.length() - 1) : string;
    }

    private String lineClearing(String string) {
        return replaceLastLineBreak(replaceAdHocTag(replaceHTMLTags(removeSpace(string))));
    }

    public String parsing (String string) throws ValidateStringIOException {
        if(validateString(string) && validateQuote(string)) return lineClearing(getSubStringQuote(string));
        return null;
    }
}
