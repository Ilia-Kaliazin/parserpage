package com.epam.parserpage.configuration;

import com.epam.parserpage.Core;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.Scanner;
import java.util.ArrayList;
import java.io.FileNotFoundException;

public class Config {
    private File file;
    private Scanner scannerFile;
    private final Core core;
    private static Config instance;
    private PrintWriter printWriter;
    private final List<String> listConfiguration = new ArrayList<>();

    private Config(Core core){
        this.core = core;
    }

    public static Config getInstance(Core core){
        if(instance != null) return instance;
        return new Config(core);
    }

    private void writeConfigToFile(String string){
        try {
            printWriter = new PrintWriter(file);
            printWriter.write("URL:" + string + "\n");
            printWriter.flush();
            run();
        } catch (FileNotFoundException e) {
            core.onIOException(e);
        }
    }

    private boolean getFile(){
        file = new File("config.properties");
        if(!file.isFile()){
            core.doesNotExistFile();
            return false;
        }
        return true;
    }

    private void addFileStringsInList (){
        while (scannerFile.hasNext()){
            listConfiguration.add(scannerFile.next());
        }
    }

    private String searchInList (String string){
        for (String str : listConfiguration) {
            if(str.contains(string)) return str;
        }
        return null;
    }

    private String getUrl(String string) {
        return searchInList(string).substring(string.indexOf(":") + 1);
    }

    public void run() {
        if(!getFile()) return;
        try {
            scannerFile = new Scanner(file);
            addFileStringsInList();
            core.setUrl(getUrl("URL:"));
        } catch (IOException e) {
            core.onIOException(e);
        }
    }

    public void createNewFileConfig(String string) {
        try {
            if (file.createNewFile()) {
                writeConfigToFile(string);
            }
        } catch (Exception e) {
            core.onIOException(e);
        }
    }
}
